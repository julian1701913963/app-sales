//  implementación sin errores de sintaxis aunque el editor opine lo contrario

agregaAlCarrito(indice) {

    let idBtnEliminar = `carrito-btneliminar-${indice}`;
    let disponibles = this.#productos[indice].disponible;
    let item = this.#porComprar.find(producto => producto.indice === indice);

    if (item) {
        document.querySelector(`#carrito-venta-${item.indice}`).scrollIntoView();
        document.querySelector(`#lstcantidad-${item.indice}`).focus();
        return;
    }

    this.#porComprar.push({
        indice,
        cantidad: 1
    });

    let elementosLista = '<option>1</option>';
    for (let i = 2; i <= disponibles; i++) {
        elementosLista += `<option>${i}</option>`;
    }

    let producto = `
        <div id="carrito-venta-${indice}"
            class="border w-full rounded mt-5 flex p-4 justify-between items-center flex-wrap">
            <div class="w-2/4">
                <h3 class="text-lg font-medium">${this.#productos[indice].referencia}</h3>
                <h4 class="text-red-700 text-xs font-bold mt-1">Sólo quedan ${disponibles} en stock </h4>
            </div>
            <div>
                <h5 class="text-2xl font-medium">
                    <sup class="text-lg text-teal-600">$</sup>
                    ${this.#productos[indice].precio}
                </h5>
                <h5 class="text-sm font-bold text-teal-800">Descuento ${this.#descuento}%</h5>
            </div>
            <div class="w-full flex justify-between mt-4">

                <button id="${idBtnEliminar}" data-indice="${indice}" 
                        class="text-red-700 hover:bg-red-100 px-2">ELIMINAR</button>

                <label class="block uppercase tracking-wide text-gray-700"
                    for="grid-first-name">
                    UNIDADES
                    <select id="lstcantidad-${indice}"
                        class="ml-3 text-sm bg-teal-700 border border-teal-200 text-white p-2 rounded leading-tight">
                        ${elementosLista}
                    </select>
                </label>
            </div>
        </div>
    `;

    document.querySelector('#carrito-elegidos').insertAdjacentHTML('beforeend', producto);
    document.querySelector('#carrito-btnpagar').style.display = '';

    document.querySelector(`#${idBtnEliminar}`).addEventListener('click', e => {
        this.eliminarDelCarrito(e.target.dataset.indice);
    });
}